package digitus.tech.todelete;

import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import digitus.tech.todelete.connectivity.InternalNetworkChangeReceiver;
import digitus.tech.todelete.connectivity.NetworkChangeReceiver;

public class BaseActivity extends AppCompatActivity {


    /**
     * This is internal BroadcastReceiver which get status from external receiver(NetworkChangeReceiver)
     */
    NetworkChangeReceiver mNetworkChangeReceiver = new NetworkChangeReceiver();
    InternalNetworkChangeReceiver mInternalNetworkChangeReceiver = new InternalNetworkChangeReceiver();


    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        registerReceiver();
    }

    /**
     * This method is responsible to register receiver with NETWORK_CHANGE_ACTION.
     */
    private void registerReceiver() {
        try {
            IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
            filter.addAction(Intent.ACTION_AIRPLANE_MODE_CHANGED);
            registerReceiver(mNetworkChangeReceiver, filter);


            IntentFilter internalFilter = new IntentFilter(InternalNetworkChangeReceiver.NETWORK_CHANGE_ACTION);
            registerReceiver(mInternalNetworkChangeReceiver, internalFilter);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        try {
            // Make sure to unregister internal receiver in onDestroy().
            unregisterReceiver(mNetworkChangeReceiver);
            unregisterReceiver(mInternalNetworkChangeReceiver);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        super.onDestroy();
    }

}
