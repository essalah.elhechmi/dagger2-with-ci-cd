package digitus.tech.todelete.connectivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class InternalNetworkChangeReceiver extends BroadcastReceiver {

    private static final String PACKAGE = "digitus.tech.todelete";

    public static final String NETWORK_CHANGE_ACTION
            = PACKAGE + ".NetworkChangeReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        Toast.makeText(context, "status = " + intent.getBooleanExtra("status", false), Toast.LENGTH_LONG)
                .show();
    }

}
